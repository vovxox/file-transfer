#!/bin/bash
sed -e 's/:[^:\/\/]/="/g;s/$/"/g;s/ *=/=/g' /app/creds/creds.yml > /app/creds/creds.sh
sed -i -e 's/access_key_id/export AWS_ACCESS_KEY_ID/g' /app/creds/creds.sh
sed -i -e 's/secret_access_key/export AWS_SECRET_ACCESS_KEY/g' /app/creds/creds.sh
sed -i -e 's/session_token/export AWS_SESSION_TOKEN/g' /app/creds/creds.sh
sed -i '/expiration/d' /app/creds/creds.sh
sed -i '/region/d' /app/creds/creds.sh
sed -i '/account_id/d' /app/creds/creds.sh
