FROM python:3
LABEL author="Ryan Park <vovxox1@gmail.com>"

RUN mkdir -p /app/creds
RUN mkdir -p /app/configs
RUN mkdir -p /app/temp
WORKDIR /app
COPY . ./
RUN pip3 install -r requirements.txt
RUN ["chmod", "+x", "./conv_yml_to_env.sh"]
RUN ["chmod", "+x", "./run.sh"]
CMD ["./run.sh"]