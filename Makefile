CONTAINER = file-transfer-py
OUT_DIR = temp
BUILD_DIR = build
KEY_DIR = keys
REGISTRY = 
PWD = $(shell pwd)
APP_IMAGE = $(REGISTRY)/$(CONTAINER)
AWS_KEYS = creds.yml

all: clean build
.PHONY: all


# build app
build:
	echo "Building container"
	docker build -t  $(APP_IMAGE) .
.PHONY: build

push: build
	echo "Pushing container"
	docker push  $(APP_IMAGE)
.PHONY: push

run: push
	echo "Running container"
	docker pull $(APP_IMAGE)
	docker run --rm -t -v $(PWD)/creds:/app/creds -e CONTROLCONFIGPATH=$(CONTROLCONFIGPATH) $(APP_IMAGE) $(CMD)
.PHONY: run

#   make clean
clean:
	sudo rm -rf $(OUT_DIR)
	sudo rm -rf $(KEY_DIR)
	sudo rm -rf $(BUILD_DIR)
	sudo rm -rf tmp
.PHONY: clean
