import os
import json
import logging
import re

from os.path import dirname, realpath
from modules.s3_operator import S3
from modules.sftp_operator import SFTP


FORMAT = '[%(asctime)s][%(name)s][%(levelname)-8s] (L:%(lineno)s) %(funcName)s: %(message)s'
logging.basicConfig(format=FORMAT, datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


"""File Operator"""


def get_temp_dir():
    parent_dir_of_file = dirname(dirname(realpath(__file__)))
    temp_dir = '%s/%s' % (parent_dir_of_file, 'temp')
    return temp_dir


def get_configs():
    parent_dir_of_file = dirname(dirname(realpath(__file__)))
    configs_dir = os.path.join(parent_dir_of_file, 'configs')
    configs_file = '%s/%s' % (configs_dir, os.environ['CONTROLCONFIGPATH'])
    return configs_file


def get_keys(key):
    parent_dir_of_file = dirname(dirname(realpath(__file__)))
    configs_dir = os.path.join(parent_dir_of_file, 'keys')
    pkey = '%s/%s' % (configs_dir, key)
    return pkey


def remove_prefix(key):
    object_dest = '%s' % re.search(r'^.*\/(.*)', key).group(1)
    return object_dest


"""End of File Operator"""


def sftp_to_s3_transfer(data):
    """ Example """
    # Establish S3 & SFTP Sessions
    sftp = SFTP(data['ftp_host'], data['ftp_username'],
                get_keys(data['ftp_pkey']), data['ftp_dir'], 22)

    s3 = S3(data['dest_s3_bucket'], data['dest_s3_prefix'])

    # List Objects in SFTP
    resp = sftp.listdir()
    for path in resp:
        temp_storage_file = '%s/%s' % (get_temp_dir(), path)
        sftp.get(path, temp_storage_file)
        s3.put(temp_storage_file, path)
        os.remove(temp_storage_file)

    # Close the SFTP session
    sftp.close()


def s3_to_sftp_transfer(data):
    """ Example """
    logger.info("Needs to be filled")


def s3_to_s3_transfer(data):
    """ Example """
    logger.info("Needs to be filled")


def main():
    try:
        with open(get_configs()) as f:
            data = json.load(f)
            logger.info("Transferring from {} to {}".format(
                data['source'], data['destination']))

            if data['source'].lower() == 's3' and data['destination'].lower() == 'sftp':
                s3_to_sftp_transfer(data)

            if data['source'].lower() == 'sftp' and data['destination'].lower() == 's3':
                sftp_to_s3_transfer(data)

            if data['source'].lower() == 's3' and data['destination'].lower() == 's3':
                s3_to_s3_transfer(data)

    except Exception:
        logger.exception("Failed to read config file")


if __name__ == '__main__':
    main()
