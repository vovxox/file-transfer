import paramiko
import os

"""
The high-level client API starts with creation of an SSHClient object.
For more direct control, pass a socket (or socket-like object) to a Transport,
and use start_server or start_client to negotiate with the remote host as either a server or client.
"""


class SFTP(object):

    def __init__(self, sftp_host, sftp_username, pem, sftp_dir, sftp_port=None):
        self._sftp = None
        self._sftp_live = False
        self._tranport_live = True
        self.sftp_host = sftp_host
        self.sftp_username = sftp_username
        self.sftp_port = sftp_port
        self.sftp_dir = sftp_dir
        self.currentDir = None

        # Use Private Key.
        if not pem:
            # Try to use default key if the Private key is not given
            if os.path.exists(os.path.expanduser('~/.ssh/id_rsa')):
                pem = '~/.ssh/id_rsa'
            elif os.path.exists(os.path.expanduser('~/.ssh/id_dsa')):
                pem = '~/.ssh/id_dsa'
            else:
                print("You haven't provided a private key.")

        try:
            pkey = paramiko.RSAKey.from_private_key_file(pem)
        except paramiko.SSHException:
            print("Bleach connection error, try again")

        # Establish SFTP connection
        self._sftp_connect(pkey)

    def _sftp_connect(self, pkey):
        """Establish the SFTP connection."""
        print("Connecting to SFTP...")
        self._transport = paramiko.SSHClient()
        self._transport.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self._transport = paramiko.Transport((self.sftp_host, self.sftp_port))
        self._transport.connect(username=self.sftp_username, pkey=pkey)

        if not self._sftp_live:
            self._sftp = paramiko.SFTPClient.from_transport(self._transport)
            self._sftp_live = True
            print("Connected")
            if self.sftp_dir is not None:
                print("Change Current Directory to {}".format(self.sftp_dir))
                self.chdir(self.sftp_dir)

    def listdir(self):
        """return a list of files for the given path"""
        self.currentDir = self.sftp_dir
        return self._sftp.listdir(self.currentDir)

    def chdir(self, path):
        """change the current working directory on the remote"""
        self._sftp.chdir(path)

    def put(self, localpath, remotepath=None):
        """Copies a file between the local host and the remote host."""
        if not remotepath:
            remotepath = os.path.split(localpath)[1]

    def get(self, remotepath, localpath):
        """Copies a file between the remote host to the local host."""
        print("Downloading in {}".format(localpath))
        self._sftp.get(remotepath, localpath)

    def close(self):
        """Closes the connection and cleans up."""
        print("Closing SFTP Connection..")
        # Close SFTP Connection.
        if self._sftp_live:
            self._sftp.close()
            self._sftp_live = False
        # Close the SSH Transport.
        if self._tranport_live:
            self._transport.close()
            self._tranport_live = False
