import threading
import boto3
import sys
import os
import aws_encryption_sdk

from boto3.s3.transfer import S3Transfer, TransferConfig
from smart_open import smart_open

"""
This module provides high level abstractions for efficient uploads/downloads. It handles several things for the user:

1. Automatically switching to multipart transfers when a file is over a specific size threshold
2. Uploading/downloading a file in parallel
3. Progress callbacks to monitor transfers
"""


class S3(object):

    def __init__(self, bucket, prefix=None, access_key=None, secret_access=None, kms_key=None):
        self._s3_live = False
        self._s3 = None
        self._s3_wormhole = None
        self._region = 'ap-southeast-2'
        self.bucket = bucket

        # Set Prefix if not None
        if prefix is not None:
            self.prefix = prefix
        else:
            self.prefix = None

        # Set KMS Key if not None
        if kms_key is not None:
            self.kms_key_provider = aws_encryption_sdk.KMSMasterKeyProvider(
                key_ids=[kms_key]
            )
        else:
            self.kms_key_provider = None

        # Set Access Key pair for external transfer if not None
        if all([access_key, secret_access]):
            self.access_key = access_key
            self.secret_access = secret_access
        else:
            self.access_key = None
            self.secret_access = None

        self._initilize()

    def _initilize(self):
        """Establish the S3 connection."""

        if not self._s3_live:
            if all([self.access_key, self.secret_access]):
                self._s3 = boto3.client(
                    's3', region_name=self._region, aws_access_key_id=self.access_key, aws_secret_access_key=self.secret_access)
            else:
                self._s3 = boto3.client('s3', region_name=self._region)
            # Warmhole to support multipart transfers with following config

            self._s3_wormhole = S3Transfer(self._s3,  config=TransferConfig(
                multipart_threshold=8 * 1024 * 1024,
                max_concurrency=10,
                num_download_attempts=10,
            ))
            self._s3_live = True

    def encrypt(self, plaintext, encrypted_file):
        """ Encrypt using KMS """
        try:
            with smart_open(encrypted_file, 'wb') as outfile:
                ciphertext, encryptor_header = aws_encryption_sdk.encrypt(
                    source=open(plaintext, 'rb'),
                    key_provider=self.kms_key_provider
                )
                print("Successfully encrypt {}".format(plaintext))
                outfile.write(ciphertext)
        except Exception as e:
            print("Failed due to {}".format(e))

    def decrypt(self, encrypted_file, decrypted_file):
        """ Decrypt using KMS """
        try:
            with smart_open(decrypted_file, 'wb') as outfile:
                plaintext, decryptor_header = aws_encryption_sdk.decrypt(
                    source=open(encrypted_file, 'rb'),
                    key_provider=self.kms_key_provider
                )
                print("Successfully decrypt {}".format(encrypted_file))
                outfile.write(plaintext)
        except Exception as e:
            print("Failed due to {}".format(e))

    def list_objects(self):
        """Get a list of all keys in an S3 bucket."""
        keys = set()
        kwargs = {'Bucket': self.bucket, 'Prefix': self.prefix}
        while True:
            resp = self._s3.list_objects_v2(**kwargs)
            for obj in resp['Contents']:
                if obj['Key'][-1] == "/":
                    continue
                keys.add(obj['Key'])

            try:
                kwargs['ContinuationToken'] = resp['NextContinuationToken']
            except KeyError:
                break
        return keys

    def get(self, key, local_path):
        """Download s3://bucket/key to local_path'."""
        try:
            self._s3_wormhole.download_file(
                bucket=self.bucket, key="{}/{}".format(self.prefix, key), filename=local_path)
        except Exception as e:
            print("Could not get {} from S3 due to {}".format(key, e))
        else:
            print("Successfully get {} from S3".format(key))

    def put(self, local_path, key):
        """Upload local_path to s3: // bucket/key and print upload progress."""
        try:
            self._s3_wormhole.upload_file(filename=local_path, bucket=self.bucket, key="{}/{}".format(
                self.prefix, key), callback=ProgressPercentage(local_path))
        except Exception as e:
            print("Could not upload {} to S3 due to {}".format(key, e))
        else:
            print("Successfully uploaded {} to S3".format(key))


class ProgressPercentage(object):
    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\r%s  %s / %s  (%.2f%%)" % (self._filename, self._seen_so_far,
                                             self._size, percentage))
            sys.stdout.flush()
