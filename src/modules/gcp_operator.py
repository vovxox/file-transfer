import googleapiclient.discovery


class GCP(object):
    def __init__(self, projectId, s3Bucket, access_key, secret_access):
        self.projectId = projectId
        self.s3Bucket = s3Bucket
        self._gcp = None
        self._gcp_live = False
        self._gcp_connect()

    def _gcp_connect(self):
        print("Connecting to Google Cloud Storage...")
        if not self._gcp_live:
            self._gcp = googleapiclient.discovery.build(
                'storagetransfer', 'v1')
            self._gcp_live = True
